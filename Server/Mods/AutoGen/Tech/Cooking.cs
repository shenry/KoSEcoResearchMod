namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Eco.Core.Utils;
    using Eco.Core.Utils.AtomicAction;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Services;
    using Eco.Shared.Utils;
    using Gameplay.Systems.Tooltip;

    [Serialized]
    [RequiresSkill(typeof(ChefSkill), 0)]    
    public partial class CookingSkill : Skill
    {
        public override string FriendlyName { get { return "Cooking"; } }
        public override string Description { get { return Localizer.Do(""); } }

        public static int[] SkillPointCost = { 1, 1, 1, 1, 1 };
        public override int RequiredPoint { get { return this.Level < this.MaxLevel ? SkillPointCost[this.Level] : 0; } }
        public override int PrevRequiredPoint { get { return this.Level - 1 >= 0 && this.Level - 1 < this.MaxLevel ? SkillPointCost[this.Level - 1] : 0; } }
        public override int MaxLevel { get { return 1; } }
    }

    [Serialized]
    public partial class CookingSkillBook : SkillBook<CookingSkill, CookingSkillScroll>
    {
        public override string FriendlyName { get { return "Cooking Skill Book"; } }
    }

    [Serialized]
    public partial class CookingSkillScroll : SkillScroll<CookingSkill, CookingSkillBook>
    {
        public override string FriendlyName { get { return "Cooking Skill Scroll"; } }
    }

    [RequiresSkill(typeof(CampfireSkill), 0)] 
    public partial class CookingSkillScrollRecipe : Recipe
    {
        public CookingSkillScrollRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<CookingSkillScroll>(),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<IronIngotItem>(typeof(ResearchEfficiencySkill), 25f/0.3f, ResearchEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<HewnLogItem>(typeof(ResearchEfficiencySkill), 20f/0.3f, ResearchEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<CampfireRoastItem>(typeof(ResearchEfficiencySkill), 10f/0.3f, ResearchEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<WheatPorridgeItem>(typeof(ResearchEfficiencySkill), 10f/0.3f, ResearchEfficiencySkill.MultiplicativeStrategy), 
            };
            this.CraftMinutes = new ConstantValue(15);

            this.Initialize("Cooking Skill Scroll", typeof(CookingSkillScrollRecipe));
            CraftingComponent.AddRecipe(typeof(ResearchTableObject), this);
        }
    }
}
