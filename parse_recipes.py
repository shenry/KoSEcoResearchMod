#!/usr/bin/python3
import sys

if __name__ == "__main__":
    file = sys.argv[1]
    with open(file) as f:
        lines = [l for l in f.readlines()]
    last = "NONE"
    new_lines = []
    for line in lines:
        if line.startswith(last):
            new_line = line.lstrip(last).rstrip("\n")
        else:
            new_line = "\n" + line.rstrip("\n")
            last = line.split(" ")[0]
        new_line = new_line.replace("/home/sydney/repos/KoSEcoResearchMod/ServerBak/Mods/", "").replace("/"," ")
        new_lines.append(new_line)

    new_lines = [line.replace("  ", " ").replace(" ", ",") for line in new_lines]
    print("\n\n")
    for line in new_lines:
        print(line, end="")
    print("\n\n")
    with open("recipes.csv", "w") as f:
        f.write("Group,Type,Product,Ingredient 1, Amount 1,Ingredient 2, Amount 2,Ingredient 3, Amount 3,Ingredient 4, Amount 4,Ingredient 5, Amount 5,\n")
        f.writelines(new_lines)
