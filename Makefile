MODNAME=KoSEcoResearchMod
VERSION=v0.2.8

MODS=Mods
AUTOGEN=$(MODS)/AutoGen
TECH=$(AUTOGEN)/Tech
BASEDIR=$(CURDIR)
SERVERDIR=$(BASEDIR)/Server
BACKUPDIR=$(BASEDIR)/ServerBak
README=$(BASEDIR)/README.md
CHANGES=$(BASEDIR)/CHANGES.diff
LICENSE=$(BASEDIR)/LICENSE.md

SERVERTECH=$(SERVERDIR)/$(TECH)
EXTRAFILES=$(README) $(CHANGES) $(LICENSE)
KEEPFILES=$(BACKUPDIR)/$(TECH)/BigStomach.cs \
		  $(BACKUPDIR)/$(TECH)/CalorieEfficiency.cs \
		  $(BACKUPDIR)/$(TECH)/StrongBack.cs \
		  $(BACKUPDIR)/$(TECH)/BowDamage.cs \
		  $(BACKUPDIR)/$(TECH)/DesertDrifter.cs \
		  $(BACKUPDIR)/$(TECH)/GrasslandGatherer.cs \
		  $(BACKUPDIR)/$(TECH)/ForestForager.cs \
		  $(BACKUPDIR)/$(TECH)/TundraTraveller.cs \
		  $(BACKUPDIR)/$(TECH)/WetlandsWanderer.cs
PACKAGE=$(BASEDIR)/$(MODNAME)_$(VERSION).zip

# STRATIGES
ADDITIVE={ 0, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f }
MULTIPLICATIVE={ 1, 1 - 0.1f, 1 - 0.2f, 1 - 0.3f, 1 - 0.4f, 1 - 0.5f, 1 - 0.6f, 1 - 0.7f, 1 - 0.8f }

# as long as the max level is <= the len(array) we good
MAXLEVEL=7
COOKINGMAX=8
COOKINGFILES=BasicBakingEfficiency.cs \
			 BasicBakingSpeed.cs \
			 CampfireCookingEfficiency.cs \
			 CampfireCookingSpeed.cs \
			 CampfireCreationsEfficiency.cs \
			 CampfireCreationsSpeed.cs \
			 CulinaryArtsEfficiency.cs \
			 CulinaryArtsSpeed.cs \
			 HomeCookingEfficiency.cs \
			 HomeCookingSpeed.cs \
			 LeavenedBakingEfficiency.cs \
			 LeavenedBakingSpeed.cs

SPEEDFILES=*Speed.cs
EFFFILES=*Efficiency.cs \
	     LoggingDamage.cs \
	     PredatoryInstincts.cs

RESEARCHCOST=0.3f

# BASE SPEED
BASESPEEDSKILLPOINT1={ 1, 1, 1, 1, 1 }
BASESPEEDSKILLPOINT2={ 2, 2, 2, 2, 2 }
BASESPEEDSKILLPOINT3={ 4, 4, 4, 4, 4 }
BASESPEEDSKILLPOINT4={ 5, 5, 5, 5, 5 }
BASESPEEDSKILLPOINT5={ 10, 10, 10, 10, 10 }

# EDDITED BASE SPEED
EDITSPEEDSKILLPOINT1={ 1, 1, 1, 1, 1, 1, 1, 1 }
EDITSPEEDSKILLPOINT2={ 2, 2, 2, 2, 2, 2, 2, 2 }
EDITSPEEDSKILLPOINT3={ 4, 4, 4, 4, 4, 4, 4, 4 }
EDITSPEEDSKILLPOINT4={ 5, 5, 5, 5, 5, 5, 5, 5 }
EDITSPEEDSKILLPOINT5={ 8, 8, 8, 8, 8, 8, 8, 8 }

# BASE EFFICIENCY
BASEEFFSKILLPOINT1={ 1, 2, 3, 4, 5 }
BASEEFFSKILLPOINT2={ 1, 5, 15, 30, 50 }
BASEEFFSKILLPOINT3={ 2, 4, 6, 8, 10 }
BASEEFFSKILLPOINT4={ 5, 10, 15, 20, 25 }
BASEEFFSKILLPOINT5={ 10, 20, 35, 55, 70 }

# EDDITED BASE EFFICIENCY
EDITEFFSKILLPOINT1={ 1, 2, 3, 4, 5, 6, 8, 10 }
EDITEFFSKILLPOINT2={ 1, 4, 8, 16, 32, 40, 45, 50 }
EDITEFFSKILLPOINT3={ 1, 2, 4, 6, 8, 10, 12, 14 }
EDITEFFSKILLPOINT4={ 4, 8, 12, 16, 20, 24, 28, 32 }
EDITEFFSKILLPOINT5={ 10, 15, 20, 30, 40, 50, 60, 80 }


help:
	@echo 'Makefile for Eco Mod                                                      '
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make help               this display                                   '
	@echo '   make clean              remove the generated files and copy over backup'
	@echo '   make pack               generate the zip                               '
	@echo '   make mod                generate the mod files                         '
	@echo '   make info               display select information from files          '
	@echo '   make recipe             display csv of recipe costs from base game     '
	@echo '   make diff               display diff of tech files                     '
	@echo '   make crafting           display the crafing elements                   '
	@echo '                                                                          '

clean:
	$(RM) -r $(SERVERDIR)
	$(RM) $(MODNAME)_v*.zip
	mkdir -p $(SERVERTECH)
	cp -r $(BACKUPDIR)/$(TECH)/*.cs $(SERVERTECH)

diff:
	diff $(BACKUPDIR)/$(TECH) $(SERVERDIR)/$(TECH) > $(CHANGES) || exit 0
	cat $(CHANGES)

pack: diff
	(cd $(SERVERDIR); zip $(PACKAGE) $(TECH)/*.cs;)
	zip -j $(PACKAGE) $(EXTRAFILES)

mod:
	echo "Edit Research Efficiencey"
	find $(SERVERTECH) -type f -name "ResearchEfficiency.cs" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(EDITEFFSKILLPOINT2);/SkillPointCost = $(EDITEFFSKILLPOINT5);/g'
	echo "Change Strategies"
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 sed -i''  -e 's/new MultiplicativeStrategy(new float\[\].*;/new MultiplicativeStrategy(new float[] $(MULTIPLICATIVE));/g'
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 sed -i''  -e 's/new AdditiveStrategy(new float\[\].*;/new AdditiveStrategy(new float[] $(ADDITIVE));/g'
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 sed -i''  -e 's/public override int MaxLevel { get { return 5; } }/public override int MaxLevel { get { return $(MAXLEVEL); } }/g'
	echo "Book Recipies to make scrolls"
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 perl -pi -e 's/CraftingElement<(\w+)Book>\(\)/CraftingElement<\1Scroll>()/g'
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 perl -pi -e 's/BookRecipe/ScrollRecipe/g'
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 perl -pi -e 's/this\.Initialize\("([\w ]+)Book"/this.Initialize("\1Scroll"/g'
	echo "Change research recipies cost"
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 perl -pi -e 's/(typeof\(ResearchEfficiencySkill\),\s*)(\d*\.?\d+)f?(,\s*)/\1\2f\/$(RESEARCHCOST)\3/g'
	echo "Edit Skillpoint costs for Speed"
	for FILE in $(SPEEDFILES); do\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASESPEEDSKILLPOINT1);/SkillPointCost = $(EDITSPEEDSKILLPOINT1);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASESPEEDSKILLPOINT2);/SkillPointCost = $(EDITSPEEDSKILLPOINT2);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASESPEEDSKILLPOINT3);/SkillPointCost = $(EDITSPEEDSKILLPOINT3);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASESPEEDSKILLPOINT4);/SkillPointCost = $(EDITSPEEDSKILLPOINT4);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASESPEEDSKILLPOINT5);/SkillPointCost = $(EDITSPEEDSKILLPOINT5);/g';\
	done
	echo "Edit Skillpoint costs for Efficency"
	for FILE in $(EFFFILES); do\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASEEFFSKILLPOINT1);/SkillPointCost = $(EDITEFFSKILLPOINT1);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASEEFFSKILLPOINT2);/SkillPointCost = $(EDITEFFSKILLPOINT2);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASEEFFSKILLPOINT3);/SkillPointCost = $(EDITEFFSKILLPOINT3);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASEEFFSKILLPOINT4);/SkillPointCost = $(EDITEFFSKILLPOINT4);/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/SkillPointCost = $(BASEEFFSKILLPOINT5);/SkillPointCost = $(EDITEFFSKILLPOINT5);/g';\
	done
	echo "Disable the costruction skills"
	find $(SERVERTECH) -type f -name "*Construction.cs" -print0 | xargs -0 sed -i'' 's/^\([^\/]\)/\/\/\1/g'
	find $(SERVERTECH) -type f -name "RoadConstruction.cs" -print0 | xargs -0 sed -i'' 's/^\/\///g'
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 perl -pi -e 's/^.*RequiresSkill.*(?<!Road)ConstructionSkill.*$$//g'
	echo "Change max level for cooking"
	for FILE in $(COOKINGFILES); do\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/public override int MaxLevel { get { return $(MAXLEVEL); } }/public override int MaxLevel { get { return $(COOKINGMAX); } }/g';\
		find $(SERVERTECH) -type f -name "$$FILE" -print0 | xargs -0 sed -i''  -e 's/public override int MaxLevel { get { return $(MAXLEVEL); } }/public override int MaxLevel { get { return $(COOKINGMAX); } }/g';\
	done
	echo "Copy over saved files"
	cp $(KEEPFILES) $(SERVERTECH)
	cd $(BASEDIR)

info:
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 grep -nP '(new \w+Strategy)|(SkillPointCost = )|(MaxLevel { get { return \d+)' | perl -pe 's/(\.\/\w+\.cs:\d+):\s+new (\w+)(?:Strategy)[^{]*{(.*)}\);/\1\t\2\t\3/gm' | perl -pe 's/(\.\/\w+\.cs:\d+):\s+[^\]]*\] (\w+)Cost[^{]*{(.*)};/\1\t\2\t\3/gm' | perl -pe 's/public override int (MaxLevel) \{ get \{ return (\d+); } }/\1\t\2/gm' | perl -pe 's#$(BASEDIR)##gm'

recipe:
	find $(BACKUPDIR) -type f -name "*.cs" -print0 | xargs -0 grep -nP 'CraftingElement<(\w+)>\((.+)\)' | perl -pe 's/([\w\/]+\/\w+).cs:[^<]*<(\w+)Item>\D*(\d+).*/\1 \2 \3/g' > temp
	python3 parse_recipes.py temp
	$(RM) temp

crafting:
	find $(SERVERTECH) -type f -name "*.cs" -print0 | xargs -0 grep -nP 'CraftingElement<(\w+)>\(\)'
