# Changes

Research Efficiency has been changed to follow end game skill point levels, so it starts at 10 points.

Construction Skills don't exist (they allow you to place bricks, etc) have been removed.

Speed nerfed from 5 to 7 levels, with an even 10% per level.

Efficiency nerfed from 5 to 7 levels, with an even 10% per level.

Cooking Efficiency and Speed maxes out at 8 level at 80%.

Researching produces SkillScrolls not SkillBooks.

Recipes for Research now have a higher base cost, so that at 70% they will cost what they cost at 0% eff in the base game.

Levels needed to upgrade skills are generally more twice as expensive.


# Unchanged

## Personal improvement

* Big Stomach
* Calorie Efficiency
* Strong Back

## Gathering

* Desert Drifter
* Tundra Traveller
* Wetlands Wanderer
* Grassland Gatherer
* Forest Forager

## Other

* Bow Damage (technical limitation?)

# Use

Use `make help` to see commands.
To make and pack

```
make clean
make mod
make pack
```

To install the game, copy the `Mod` folder in the zip and paste it into your `Eco_Data/Server` folder to overwrite the needed files.

# About

Made by Sydney (FSMimp). Values from KatherineOfSky Discord Eco Moderators.

This is a work in progress, which is why commands are not consistent.

Special thanks to Caledorn, Alnilana, and Wally1169

https://gitlab.com/shenry/KoSEcoResearchMod


# Versions

v0.1.0 - KoS Server 4

v0.2.x - KoS Server 5
